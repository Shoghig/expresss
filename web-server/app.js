const express = require("express");
const hbs = require("hbs");
const fs = require("fs");

var app = express();

hbs.registerPartials(__dirname + '/views/partials')
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'hbs');

hbs.registerHelper('getCurrentYear', () => {
    return new Date().getFullYear();
});
hbs.registerHelper('screamIt', (Text) => {
    return  Text.toUpperCase();
})


app.get("/", (req, res) => {
    res.render("home.hbs");
});

app.get("/about", (req, res) =>{
    res.render("about.hbs")
});
app.get("/news", (req, res) =>{
    res.render("News.hbs")
})
app.get("/contact", (req, res) =>{
    res.render("Contact.hbs")
})
app.listen(3000);
